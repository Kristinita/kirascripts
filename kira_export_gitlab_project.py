"""Script for exporting GitLab project.

[INFO] Using python-gitlab:
https://python-gitlab.readthedocs.io/en/latest/
[INFO] What will be exported and not exported:
https://docs.gitlab.com/ee/user/project/settings/import_export.html#exported-contents
[INFO] GitLab API:
https://docs.gitlab.com/ee/api/project_import_export.html
"""
# @Author: SashaChernykh
# @Date: 2019-09-01 16:49:06
# @Last Modified time: 2019-09-02 11:28:59
import os
import time
import gitlab

# [INFO] Connect to GitLab server:
# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
# [INFO] Create personal access token:
# https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
# [NOTE] I select “api” scope for my token:
# https://forum.gitlab.com/t/question-scopes-which-scopes-required-for-project-exporting/29542
PRIVATE_TOKEN = os.environ['TOKEN_PYTHON_GITLAB_EXPORT_PROJECT']
GL = gitlab.Gitlab('https://gitlab.com', private_token=PRIVATE_TOKEN)

GL.auth()

# [NOTE] 3 stages required for exporting:
# 1. Creating the export, 2. Waiting finished status, 3. Downloading
# [INFO] Create the export:
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html#id3
# [INFO] Get GitLab project id:
# https://stackoverflow.com/a/53126068/5951529
P = GL.projects.get(14042448)
EXPORT = P.exports.create({})

# [INFO] Wait for the 'finished' status
EXPORT.refresh()
while EXPORT.export_status != 'finished':
    time.sleep(1)
    EXPORT.refresh()

# [INFO] Download the result
# [NOTE] Create file “kiraexport.tgz” in your path → this script overwrite it:
# [INFO] 7zip can unpack “tar.gz” in Windows
with open('D:\\KiraBackup\\GitLab\\kiraexport.tar.gz', 'wb') as f:
    EXPORT.download(streamed=True, action=f.write)
